## About this TP
This was created just for fun, and it will not be regularly updated or whatsoever. Its main goal is to replace minecraft's default sounds, but some particle replacements are planned as well.

## Suggestions
Feel free to share your suggestions in the form of an issue. 

## Installation (git bash required)

1. Create a folder somewhere on your machine where the TP should be saved to
2. Open git bash and navigate to the folder created in step 1
3. Copy the Repo's location to your clipboard by clicking on Clone -> SSH -> Clipboard-Icon from the Repo's Dashboard
4. Type "git clone " and paste the location from your clipboard into git bash and hit enter
5. Once it's finished, you can zip the folder containing the TP's files and move it to your .minecraft/texturepacks folder
